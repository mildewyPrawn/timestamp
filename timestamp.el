;; -*- lexical-binding: t; -*-

(defvar timestamp--start-time nil)
(defvar timestamp--buffer nil)
(defcustom timestamp-dir "~/.timestamps/"
  "Default directory for store the timestamps. This is ignored if
  the default name (dd-mm-yyyy) is ignored")
(defcustom timestamp-ext ".org"
  "Default   file  extension   for   the   file  containing   the
  timestamps. This is ignored if the default name (dd-mm-yyyy) is
  ignored")

(defun timestamp-start (flag &optional buff-name)
  "Start the timestamps

https://stackoverflow.com/questions/25447312/emacs-lisp-how-to-use-interactive-for-conditional-arguments
interactive for conditionals."
  (interactive
   (let (flag buff-name)
     (setq flag (y-or-n-p "Default name (dd-mm-yyyy): "))
     (if (not flag)
         (setq buff-name (read-string "New buffer name: "))
       (setq buff-name (concat timestamp-dir (format-time-string "%d-%m-%Y") timestamp-ext)))
     (list flag buff-name)))
  (setq timestamp--buffer (find-file-noselect buff-name))
  (setq timestamp--start-time (float-time))
  (let ((inhibit-message t))
    (with-current-buffer timestamp--buffer
      (delete-region (point-min) (point-max))
      (save-buffer)))
  ;; (message "Started timestamp at %s" buff-name))
  (message "Started timestamp at %s" timestamp--buffer))

(defun format--timestamp (title)
  "gives format for time.

https://wilkesley.org/~ian/xah/emacs/elisp_datetime.html"
  (format "- %s %s\n"
          (format-seconds "%h:%z%.2m:%.2s" (- (float-time)
                                              timestamp--start-time))
          title))

(defun timestamp-add (title)
  "Add a timestamp

 get           the           user           input           from:
 https://wilkesley.org/~ian/xah/emacs/elisp_idioms_prompting_input.html"
  (interactive "sEnter the new title: ")
  (let ((inhibit-message t))
    (append-to-file (format--timestamp title) nil (buffer-file-name timestamp--buffer))
    (save-buffer)))

(defun timestamp-status ()
  "Status of the current timestamp if exists."
  (interactive)
  (if (not timestamp--start-time)
      (message "There is no current timestamp.")
    (message "The stamp started %s minutes ago." (format-seconds
                                                    "%h:%z%.2m:%.2s"
                                                    (- (float-time)
                                                       timestamp--start-time)))))

(defun timestamp-end ()
  "End the timestamp."
  (interactive)
  (setq timestamp--start-time nil)
  (message "Timestamp has ended."))
